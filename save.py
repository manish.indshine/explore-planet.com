import current_date as cdate
import json
import os

def save_im_id(location, image_ids):
    current_date = cdate.current_date()
    file = os.path.join(location, current_date[0]+current_date[1]+current_date[2]+'.json')
    image_dict = {}
    
    for i in range(len(image_ids)):
        image_dict[str(i)] = image_ids[i]
        
    with open(file, 'w') as outfile:
        json.dump(image_dict, outfile)
    
