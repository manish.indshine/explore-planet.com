import current_date as cdate
import requests
from requests.auth import HTTPBasicAuth


def check_data(start_date, end_date, geojson_geometry, item_type, APIkey):
    current_date = cdate.current_date()

#    start_date = '2018-08-31T00:00:00.000Z'
#    end_date = '2018-09-10T00:00:00.000Z'
#
#    end_date = current_date[0] + '-' + current_date[1] + '-' + current_date[2] + 'T00:00:00.000Z'

    # get images that overlap with our AOI
    geometry_filter = {
        'type': 'GeometryFilter',
        'field_name': 'geometry',
        'config': geojson_geometry
    }

    # get images acquired within a date range
    date_range_filter = {
        'type': 'DateRangeFilter',
        'field_name': 'acquired',
        'config': {
            'gte': start_date,
            'lte': end_date
        }
    }

    # only get images which have <50% cloud coverage
    cloud_cover_filter = {
        'type': 'RangeFilter',
        'field_name': 'cloud_cover',
        'config': {
            'lte': 0.5
        }
    }

    # combine our geo, date, cloud filters
    combined_filter = {
        'type': 'AndFilter',
        'config': [geometry_filter, date_range_filter]
    }

    # API Key stored as an env variable

    # API request object
    search_request = {
        'interval': 'day',
        'item_types': [item_type],
        'filter': combined_filter
    }

    # fire off the POST request
    search_result = \
        requests.post(
            'https://api.planet.com/data/v1/quick-search',
            auth=HTTPBasicAuth(APIkey, ''),
            json=search_request)

    # extract image IDs only
    image_ids = [feature['id'] for feature in search_result.json()['features']]
    return image_ids
