import datetime

def month2num(month):
    if month == 'January':
        num = '01'
    elif month == 'February':
        num = '02'
    elif month == 'March':
        num = '03'
    elif month == 'April':
        num = '04'
    elif month == 'May':
        num = '05'
    elif month == 'June':
        num = '06'
    elif month == 'July':
        num = '07'
    elif month == 'August':
        num = '08'
    elif month == 'September':
        num = '09'
    elif month == 'October':
        num = '10'
    elif month == 'November':
        num = '11'
    elif month == 'Decemeber':
        num = '12'
    return num
    
def current_date():    
    year = datetime.date.today().strftime("%Y")
    month = month2num(datetime.date.today().strftime("%B"))
    day = datetime.date.today().strftime("%d")
    return year, month, day