import urllib
import os
import json
import requests
from requests.auth import HTTPBasicAuth
import sys
import download as dwn
import check
import general as gn
import save
import current_date as cdate
import time

# Item Type to choose fromPSScene3Band
# PSScene4Band
# PSOrthoTile
# REOrthoTile
# REScene
# SkySatScene 0.72m
# SkySatCollect
# Landsat8L1G
# Sentinel2L1C

item_type = 'PSOrthoTile'
# API Key
PLANET_API_KEY = '483daf302afa4e42aa0f9fcbe8e09dd3'
path_image_id = 'data_list'
path_output = 'data'

# Coordinates of location to be downloaded
geojson_geometry = {
    'type': 'Polygon',
    'coordinates': [
        [
            [83.02814483642578, 25.324554437864542],
            [83.03501129150389, 25.324554437864542],
            [83.03501129150389, 25.328976548256637],
            [83.02814483642578, 25.328976548256637],
            [83.02814483642578, 25.324554437864542]
        ]
    ]
}

# Current date and start data of data to be checked
current_date = cdate.current_date()
start_date = '2018-08-31T00:00:00.000Z'
# end_date = '2018-09-10T00:00:00.000Z'
end_date = current_date[0] + '-' + current_date[1] + \
    '-' + current_date[2] + 'T00:00:00.000Z'

# Requesting all the image ids
image_ids = check.check_data(
    start_date, end_date, geojson_geometry, item_type, PLANET_API_KEY)

save.save_im_id(path_image_id, image_ids)

found = 0
prev = 1
prev_data = []
self_link = []
activation_link = []
activation_status = []

curr_date_json = current_date[0] + current_date[1] + current_date[2] + '.json'
curr_data = gn.load_json(os.path.join(path_image_id, curr_date_json))

while found == 0 or prev < 30:
    minus = str(int(current_date[2])-prev)
    if len(minus) < 2:
        minus = '0' + minus

    prev_date = current_date[0] + current_date[1] + minus + '.json'
    prev = prev + 1
    if prev > 30:
        break

    if os.path.isfile(os.path.join(path_image_id, prev_date)) == True:
        prev_data = gn.load_json(os.path.join(path_image_id, prev_date))
        break

down_data = gn.compare_data(curr_data, prev_data)

if down_data == []:
    print('All data already downloaded')
    sys.exit()

else:
    for i in range(len(image_ids)):
        while 1:
            
            id = image_ids[i]
            id_url = 'https://api.planet.com/data/v1/item-types/{}/items/{}/assets'.format(
                item_type, id)
            time.sleep(20)
            
            # Returns JSON metadata for assets in this ID. Learn more: planet.com/docs/reference/data-api/items-assets/#asset
            result = gn.result(id_url, PLANET_API_KEY)
    
            # List of asset types available for this particular satellite image
            print(result.json()['visual']['status'])
    
            links = result.json()[u'visual']['_links']
            self_link.append(links['_self'])
            activation_link.append(links['activate'])
    
            # Request activation of the 'visual' asset:
            activate_result = gn.activate_result(activation_link[i], PLANET_API_KEY)
    
            # Checking its activation status
            activation_status_result = gn.activation_status(
                self_link[i], PLANET_API_KEY)
    
            # Keeping all the activation status
            activation_status.append(activation_status_result.json()['status'])
            print(activation_status_result.json()['status'])
            
            # If activated then download the data
            if activation_status_result.json()['status'] == 'active':
                # Image can be downloaded by making a GET with your Planet API key, from here:
                download_link = activation_status_result.json()['location']
                dwn.dwn_data(download_link, os.path.join(path_output,id + '.tif'))
                print('Downloading %s completed' % (id))
                break
                
    sys.exit()