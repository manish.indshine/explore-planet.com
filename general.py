import requests
from requests.auth import HTTPBasicAuth
import json
import yaml


def activate_result(activation_link, PLANET_API_KEY):
    activate_result = \
        requests.get(
            activation_link,
            auth=HTTPBasicAuth(PLANET_API_KEY, '')
        )

    return activate_result


def activation_status(self_link, PLANET_API_KEY):
    activation_status_result = \
        requests.get(
            self_link,
            auth=HTTPBasicAuth(PLANET_API_KEY, '')
        )
    return activation_status_result


def result(url, PLANET_API_KEY):
    result = \
        requests.get(
            url,
            auth=HTTPBasicAuth(PLANET_API_KEY, '')
        )
    return result


def load_json(file):
    return yaml.safe_load(open(file))


def compare_data(curr_data, prev_data):
    down_data = []
    if prev_data == []:
        down_data = curr_data

    else:
        for j in range(len(curr_data)):
            found = 0
            for k in range(len(prev_data)):
                if curr_data[j] == prev_data[k]:
                    found = 1
            if found == 0:
                down_data.append(curr_data[j])

    return down_data
